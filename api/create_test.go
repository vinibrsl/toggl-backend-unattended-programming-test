package api

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func request(method string, url string, body string) (*httptest.ResponseRecorder, *http.Request) {
	requestBodyReader := strings.NewReader(body)
	request := httptest.NewRequest(method, url, requestBodyReader)
	request.Header.Set("Content-Type", "application/json")
	writer := httptest.NewRecorder()

	return writer, request
}

func readJsonResponse(test *testing.T, writer *httptest.ResponseRecorder) map[string]interface{} {
	response := writer.Result()
	defer response.Body.Close()

	bodyString, err := ioutil.ReadAll(response.Body)
	if err != nil {
		test.Fatalf("expected success got error %v", err)
	}

	var body map[string]interface{}
	json.Unmarshal(bodyString, &body)
	return body
}

func TestCreate(test *testing.T) {
	writer, request := request(http.MethodPost, "/decks", "{}")
	HandleCreate(writer, request)
	response := readJsonResponse(test, writer)

	if response["deck_id"] == nil {
		test.Fatal("expected id not to be nil")
	}
	if response["remaining"] != 52.0 {
		test.Fatalf("expected remaining to be %v got %v", 52, response["remaining"])
	}
	if response["shuffled"] != false {
		test.Fatalf("expected shuffled to be %v got %v", false, response["shuffled"])
	}
}

func TestCreateShuffled(test *testing.T) {
	writer, request := request(http.MethodPost, "/decks", `{"shuffled":true}`)
	HandleCreate(writer, request)
	response := readJsonResponse(test, writer)

	if response["deck_id"] == nil {
		test.Fatal("expected id not to be nil")
	}
	if response["remaining"] != 52.0 {
		test.Fatalf("expected remaining to be %v got %v", 3.0, response["remaining"])
	}
	if response["shuffled"] != true {
		test.Fatalf("expected shuffled to be %v got %v", true, response["shuffled"])
	}
}

func TestCreateWithCustomCards(test *testing.T) {
	writer, request := request(http.MethodPost, "/decks", `{"cards":["10H","AS","2H"]}`)
	HandleCreate(writer, request)
	response := readJsonResponse(test, writer)

	if response["deck_id"] == nil {
		test.Fatal("expected id not to be nil")
	}
	if response["remaining"] != 3.0 {
		test.Fatalf("expected remaining to be %v got %v", 3.0, response["remaining"])
	}
	if response["shuffled"] != false {
		test.Fatalf("expected shuffled to be %v got %v", false, response["shuffled"])
	}
}

func TestCreateInvalidFormat(test *testing.T) {
	writer, request := request(http.MethodPost, "/decks", `d$"cards":["10H","AS","2"j]}`)
	HandleCreate(writer, request)
	response := readJsonResponse(test, writer)

	if response["error"] != "Request format is not valid" {
		test.Fatal("expected invalid request format error")
	}
}
