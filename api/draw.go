package api

import (
	"github.com/gorilla/mux"
	"net/http"
	"vinibrasil.com/toggl_assessment/model"
	"vinibrasil.com/toggl_assessment/storage"
)

type drawCardsRequest struct {
	Number int `json:"number"`
}

type drawCardsResponse struct {
	Cards []cardResponse `json:"cards"`
}

type cardResponse struct {
	Value string `json:"value"`
	Suit  string `json:"suit"`
	Code  string `json:"code"`
}

func HandleDrawCards(writer http.ResponseWriter, request *http.Request) {
	var data drawCardsRequest
	if err := decodeJSON(writer, request, &data); err != nil {
		return
	}

	id, _ := mux.Vars(request)["id"]
	deck, found := storage.Get(id)
	if found == false {
		respondWithError(writer, http.StatusNotFound, "Deck does not exist")
		return
	}

	cards, err := deck.Draw(data.Number)
	if err != nil {
		respondWithError(writer, http.StatusUnprocessableEntity, err.Error())
		return
	}

	storage.Upsert(deck)

	response := drawCardsResponse{Cards: transformCardsToResponse(cards)}
	respondWithJSON(writer, http.StatusOK, response)
}

func transformCardsToResponse(cards []model.Card) []cardResponse {
	var response []cardResponse
	for _, card := range cards {
		item := cardResponse{
			Value: model.CardValuesToEnglish[card.Value],
			Suit:  model.CardSuitsToEnglish[card.Suit],
			Code:  card.ToString(),
		}

		response = append(response, item)
	}

	return response
}
