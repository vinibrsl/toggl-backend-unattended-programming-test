package api

import (
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"os"
	"strconv"
)

func RunServer() {
	router := mux.NewRouter()
	router.Use(logRequest)
	router.HandleFunc("/decks", HandleCreate).Methods("POST")
	router.HandleFunc("/decks/{id}", HandleGet).Methods("GET")
	router.HandleFunc("/decks/{id}/draw", HandleDrawCards).Methods("POST")

	port := getServerPort()
	log.Printf("Listening to %s...", port)
	http.ListenAndServe(port, router)
}

func logRequest(next http.Handler) http.Handler {
	return http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
		log.Printf("Processing %v %v", request.Method, request.RequestURI)
		next.ServeHTTP(writer, request)
	})
}

const defaultPort = "1210"

func getServerPort() string {
	port := os.Getenv("TOGGL_PORT")

	if _, err := strconv.Atoi(port); err != nil {
		log.Printf("TOGGL_PORT environment variable is invalid or empty. Defaulting to %v", defaultPort)
		port = defaultPort
	}

	return ":" + port
}
