package api

import (
	"github.com/gorilla/mux"
	"net/http"
	"testing"
	"vinibrasil.com/toggl_assessment/model"
	"vinibrasil.com/toggl_assessment/storage"
)

func TestDraw(test *testing.T) {
	deck := model.NewDeck(false, nil)
	storage.Upsert(deck)

	url := "/decks/" + deck.Id.String() + "/draw"
	writer, request := request(http.MethodPost, url, `{"number":1}`)
	request = mux.SetURLVars(request, map[string]string{"id": deck.Id.String()})

	HandleDrawCards(writer, request)
	response := readJsonResponse(test, writer)

	if len(response["cards"].([]interface{})) != 1 {
		test.Fatal("expected cards to have only one item")
	}
}

func TestDrawHighNumber(test *testing.T) {
	deck := model.NewDeck(false, nil)
	storage.Upsert(deck)

	url := "/decks/" + deck.Id.String() + "/draw"
	writer, request := request(http.MethodPost, url, `{"number":500}`)
	request = mux.SetURLVars(request, map[string]string{"id": deck.Id.String()})

	HandleDrawCards(writer, request)
	response := readJsonResponse(test, writer)

	if response["error"] != "There are not enough cards to draw" {
		test.Fatal("expected not enough cards error")
	}
}

func TestDrawInvalidNumber(test *testing.T) {
	deck := model.NewDeck(false, nil)
	storage.Upsert(deck)

	url := "/decks/" + deck.Id.String() + "/draw"
	writer, request := request(http.MethodPost, url, `{"number":-50}`)
	request = mux.SetURLVars(request, map[string]string{"id": deck.Id.String()})

	HandleDrawCards(writer, request)
	response := readJsonResponse(test, writer)

	if response["error"] != "Number of cards to draw must be greater than zero" {
		test.Fatal("expected not enough cards error")
	}
}

func TestDrawInvalidId(test *testing.T) {
	url := "/decks/75c4aa76-a74d-4b43-a059-d49a4e0f59dc/draw"
	writer, request := request(http.MethodPost, url, `{"number":-50}`)
	request = mux.SetURLVars(request, map[string]string{"id": "75c4aa76-a74d-4b43-a059-d49a4e0f59dc"})

	HandleDrawCards(writer, request)
	response := readJsonResponse(test, writer)

	if response["error"] != "Deck does not exist" {
		test.Fatal("expected deck does not exist error")
	}
}

func TestDrawInvalidFormat(test *testing.T) {
	url := "/decks/75c4aa76-a74d-4b43-a059-d49a4e0f59dc/draw"
	writer, request := request(http.MethodPost, url, `{numer\":5}`)
	request = mux.SetURLVars(request, map[string]string{"id": "75c4aa76-a74d-4b43-a059-d49a4e0f59dc"})

	HandleDrawCards(writer, request)
	response := readJsonResponse(test, writer)

	if response["error"] != "Request format is not valid" {
		test.Fatal("expected invalid request format error")
	}
}
