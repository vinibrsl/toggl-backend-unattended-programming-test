package api

import (
	"encoding/json"
	"io"
	"log"
	"net/http"
)

func decodeJSON(writer http.ResponseWriter, request *http.Request, data any) error {
	defer request.Body.Close()
	decoder := json.NewDecoder(request.Body)

	err := decoder.Decode(&data)
	if err == io.EOF {
		// EOF decode error means the content was empty. This is not necessarily an error, so it
		// delegates this decision to the caller (e.g. HandleDrawCards)
		return nil
	} else if err != nil {
		log.Printf("Error processing create request: %s", err.Error())
		respondWithError(writer, http.StatusBadRequest, "Request format is not valid")
		return err
	}

	return nil
}

func respondWithError(writer http.ResponseWriter, httpCode int, message string) {
	respondWithJSON(writer, httpCode, map[string]string{"error": message})
}

func respondWithJSON(writer http.ResponseWriter, httpCode int, payload interface{}) {
	response, _ := json.Marshal(payload)

	writer.Header().Set("Content-Type", "application/json")
	writer.WriteHeader(httpCode)
	writer.Write(response)
}
