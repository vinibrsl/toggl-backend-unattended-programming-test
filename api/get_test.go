package api

import (
	"github.com/gorilla/mux"
	"net/http"
	"testing"
	"vinibrasil.com/toggl_assessment/model"
	"vinibrasil.com/toggl_assessment/storage"
)

func TestGet(test *testing.T) {
	deck := model.NewDeck(false, nil)
	storage.Upsert(deck)

	url := "/decks/" + deck.Id.String()
	writer, request := request(http.MethodPost, url, "")
	request = mux.SetURLVars(request, map[string]string{"id": deck.Id.String()})

	HandleGet(writer, request)
	response := readJsonResponse(test, writer)

	if response["remaining"] != 52.0 {
		test.Fatal("expected remaining to be 52")
	}
	if response["deck_id"] != deck.Id.String() {
		test.Fatalf("expected id to be %v but got %v", deck.Id.String(), response["deck_id"])
	}
	if response["shuffled"] != false {
		test.Fatalf("expected shuffled to be %v but got %v", deck.IsShuffled, response["shuffled"])
	}
}

func TestGetInvalidId(test *testing.T) {
	url := "/decks/75c4aa76-a74d-4b43-a059-d49a4e0f59dc/draw"
	writer, request := request(http.MethodPost, url, "")
	request = mux.SetURLVars(request, map[string]string{"id": "75c4aa76-a74d-4b43-a059-d49a4e0f59dc"})

	HandleGet(writer, request)
	response := readJsonResponse(test, writer)

	if response["error"] != "Deck does not exist" {
		test.Fatal("expected deck does not exist error")
	}
}
