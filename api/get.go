package api

import (
	"github.com/gorilla/mux"
	"net/http"
	"vinibrasil.com/toggl_assessment/storage"
)

type getDeckResponse struct {
	Id        string         `json:"deck_id"`
	Shuffled  bool           `json:"shuffled"`
	Remaining int            `json:"remaining"`
	Cards     []cardResponse `json:"cards"`
}

func HandleGet(writer http.ResponseWriter, request *http.Request) {
	id, _ := mux.Vars(request)["id"]
	deck, found := storage.Get(id)
	if found == false {
		respondWithError(writer, http.StatusNotFound, "Deck does not exist")
		return
	}

	response := getDeckResponse{
		Id:        deck.Id.String(),
		Shuffled:  deck.IsShuffled,
		Remaining: deck.Remaining(),
		Cards:     transformCardsToResponse(deck.Cards),
	}

	respondWithJSON(writer, http.StatusOK, response)
}
