package api

import (
	"net/http"
	"vinibrasil.com/toggl_assessment/model"
	"vinibrasil.com/toggl_assessment/storage"
)

type createDeckRequest struct {
	Shuffled bool     `json:"shuffled"`
	Cards    []string `json:"cards"`
}

type createDeckResponse struct {
	Id        string `json:"deck_id"`
	Shuffled  bool   `json:"shuffled"`
	Remaining int    `json:"remaining"`
}

func HandleCreate(writer http.ResponseWriter, request *http.Request) {
	var data createDeckRequest
	if err := decodeJSON(writer, request, &data); err != nil {
		return
	}

	cards, err := data.parseCards()
	if err != nil {
		respondWithError(writer, http.StatusBadRequest, err.Error())
		return
	}

	deck := model.NewDeck(data.Shuffled, cards)
	deck = storage.Upsert(deck)

	response := createDeckResponse{Id: deck.Id.String(), Shuffled: deck.IsShuffled, Remaining: deck.Remaining()}
	respondWithJSON(writer, http.StatusOK, response)
}

func (data *createDeckRequest) parseCards() ([]model.Card, error) {
	cards := make([]model.Card, 0, len(data.Cards))
	var err error

	for _, unparsed := range data.Cards {
		card, newCardErr := model.NewCard(unparsed)
		err = newCardErr
		cards = append(cards, card)
	}

	return cards, err
}
