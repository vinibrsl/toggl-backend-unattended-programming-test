package storage

import (
	"github.com/google/uuid"
	"testing"
	"vinibrasil.com/toggl_assessment/model"
)

func TestUpsert(test *testing.T) {
	deck := model.NewDeck(false, nil)
	Upsert(deck)

	foundDeck, found := Get(deck.Id.String())
	if found != true {
		test.Fatal("expected to have found the deck by id")
	}
	if foundDeck.Id != deck.Id {
		test.Fatal("expected to have the same id")
	}

	deck.IsShuffled = true
	Upsert(deck)

	foundDeck, found = Get(deck.Id.String())
	if found != true {
		test.Fatal("expected to have found the deck by id")
	}
	if foundDeck.IsShuffled != true {
		test.Fatal("expected shuffle to be true")
	}
}

func TestGetWithUnexistingId(test *testing.T) {
	_, found := Get(uuid.New().String())
	if found != false {
		test.Fatal("expected found to be false")
	}
}

func TestGetWithInvalidId(test *testing.T) {
	_, found := Get("invalid id")
	if found != false {
		test.Fatal("expected found to be false")
	}
}
