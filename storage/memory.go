package storage

import (
	"github.com/google/uuid"
	"vinibrasil.com/toggl_assessment/model"
)

var decks = map[uuid.UUID]model.Deck{}

// Gets a deck by ID. Returns a tuple of deck and a boolean whether the deck exists or not.
func Get(id string) (deck model.Deck, found bool) {
	uuid, err := uuid.Parse(id)
	if err != nil {
		return deck, false
	}

	deck, found = decks[uuid]
	return deck, found
}

// Inserts a new deck or updates if exists.
func Upsert(deck model.Deck) model.Deck {
	decks[deck.Id] = deck
	return deck
}
