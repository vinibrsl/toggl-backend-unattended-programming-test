# Toggl Backend Unattended Programming Test

This is [my](mailto:vini@hey.com) submission to the Backend Developer role at Toggl. The assesment
specification is [available on Notion](https://toggl.notion.site/Toggl-Backend-Unattended-Programming-Test-015a95428b044b4398ba62ccc72a007e).

## Caveats
* 🏆 This is my first time writing Golang and it was really fun to learn
* 🧙 Code is definitely more verbose than Elixir or Ruby but this is not necessarily bad as it
  removes most of the "hidden/magic" behavior

## Design
Code is divided in four packages: `api`, `storage`, `model`, and of course `main`.
1. `model` defines the `Card` and `Deck` schemas and knows how to change them (e.g. when shuffling
   a deck or drawing a card from a deck)
2. `storage` persists the models for future use (in memory)
3. `api` knows how to spin up the web server, handles requests and interacts with `model` and
   `storage`
4. `main` calls `api` to start up the web server

## Running
The easiest way of running the app is using Docker.

```
% docker build -t viniciusbrasil-toggl .
% docker run -p 80:1212 viniciusbrasil-toggl
# You should be up and running in port 1212 (replace it if you want!).
```

If you want to setup it locally, you'll need Go 1.18.

```
% go mod tidy
% go run main.go
# 2022/06/16 13:29:22 TOGGL_PORT environment variable is invalid or empty. Defaulting to 1210
# 2022/06/16 13:29:22 Listening to :1210...

% TOGGL_PORT=80 go run main.go
# 2022/06/16 13:29:22 Listening to :80...
```

To run the test suite, just run:

```
% go test ./...
# ?   	vinibrasil.com/toggl_assessment	[no test files]
# ok  	vinibrasil.com/toggl_assessment/api	0.845s
# ok  	vinibrasil.com/toggl_assessment/model	0.695s
# ok  	vinibrasil.com/toggl_assessment/storage	0.519s
```

## API
The API specification is simple and follows the [assesment description](https://toggl.notion.site/Toggl-Backend-Unattended-Programming-Test-015a95428b044b4398ba62ccc72a007e).
Here are some examples using good and old `curl`.

### Creating a deck
```
% curl --location --request POST 'http://localhost:1210/decks' \
       --header 'Content-Type: application/json' \
       --data-raw '{"shuffled":false}'

# {
#     "deck_id": "f2ae28cb-115d-443b-b425-bd7507083ccd",
#     "shuffled": false,
#     "remaining": 52
# }
```

### Creating a custom card deck
```
% curl --location --request POST 'http://localhost:1210/decks' \
       --header 'Content-Type: application/json' \
       --data-raw '{"shuffled": false,"cards": ["AS", "KH", "8C"]}'

# {
#     "deck_id": "09668fc9-36b8-439c-bafa-57f73762314c",
#     "shuffled": false,
#     "remaining": 3
# }
```

### Retrieving a deck
```
% curl --location --request GET 'http://localhost:1210/decks/09668fc9-36b8-439c-bafa-57f73762314c'

# {
#     "deck_id": "09668fc9-36b8-439c-bafa-57f73762314c",
#     "shuffled": false,
#     "remaining": 3,
#     "cards": [
#         {
#             "value": "ACE",
#             "suit": "SPADES",
#             "code": "AS"
#         },
#         {
#             "value": "KING",
#             "suit": "HEARTS",
#             "code": "KH"
#         },
#         {
#             "value": "8",
#             "suit": "CLUBS",
#             "code": "8C"
#         }
#     ]
# }
```

### Draw cards from a deck
```
% curl --location --request POST 'http://localhost:1210/decks/09668fc9-36b8-439c-bafa-57f73762314c/draw' \
       --header 'Content-Type: application/json' \
       --data-raw '{"number":1}'

# {
#     "cards": [
#         {
#             "value": "ACE",
#             "suit": "SPADES",
#             "code": "AS"
#         }
#     ]
# }
```
