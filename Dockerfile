FROM golang:1.18.3
LABEL maintainer="Vinicius Brasil"

WORKDIR /go/src/app
COPY . .

RUN go get -d -v ./...
RUN go install -v ./...

ENV TOGGL_PORT=80
RUN go build -o ./app
EXPOSE 80

CMD [ "./app" ]
