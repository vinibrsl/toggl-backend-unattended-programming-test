package model

import (
	"errors"
)

const (
	spades   string = "S"
	diamonds        = "D"
	clubs           = "C"
	hearts          = "H"
)

var cardSuits = []string{spades, diamonds, clubs, hearts}
var CardSuitsToEnglish = map[string]string{
	spades:   "SPADES",
	diamonds: "DIAMONDS",
	clubs:    "CLUBS",
	hearts:   "HEARTS",
}

const (
	ace   string = "A"
	two          = "2"
	three        = "3"
	four         = "4"
	five         = "5"
	six          = "6"
	seven        = "7"
	eight        = "8"
	nine         = "9"
	ten          = "10"
	joker        = "J"
	queen        = "Q"
	king         = "K"
)

var cardValues = []string{ace, two, three, four, five, six, seven, eight, nine, ten, joker, queen, king}
var CardValuesToEnglish = map[string]string{
	ace:   "ACE",
	two:   "2",
	three: "3",
	four:  "4",
	five:  "5",
	six:   "6",
	seven: "7",
	eight: "8",
	nine:  "9",
	ten:   "10",
	joker: "JOKER",
	queen: "QUEEN",
	king:  "KING",
}

// The Card struct is a tuple of suit and value, e.g. ace of spades is represented as
// Card{Suit: "S", Value: "A"}
type Card struct {
	Suit  string
	Value string
}

var invalidCardFormatError = errors.New("Card format must be {A-K}{S-H}, e.g. AS or 2C")

// Creates a Card from a value-suit string, e.g. ace of spades is represented as "AS" and ten of
// hearts as "10H"
func NewCard(stringForm string) (Card, error) {
	lastIndex := len(stringForm) - 1
	suit := stringForm[lastIndex:]
	value := stringForm[:lastIndex]

	if !contains(suit, cardSuits) {
		return Card{}, invalidCardFormatError
	}

	if !contains(value, cardValues) {
		return Card{}, invalidCardFormatError
	}

	return Card{Suit: suit, Value: value}, nil
}

// Returns a string representation of the card, e.g. ace of spades is represented as "AS"
func (card *Card) ToString() string {
	return card.Value + card.Suit
}

func contains(term string, slice []string) bool {
	for _, current := range slice {
		if current == term {
			return true
		}
	}
	return false
}
