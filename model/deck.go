package model

import (
	"errors"
	"github.com/google/uuid"
	"math/rand"
	"time"
)

// The Deck struct represents an ordered list of playing cards
type Deck struct {
	Id         uuid.UUID
	Cards      []Card
	IsShuffled bool
}

// This function creates a new Deck. If the cards argument is empty or nil, it defaults to a
// standard 52-card deck of French playing cards including all thirteen ranks in each of the four
// suits. If isShuffled is set to true, it rearranges the list of playing cards randomly.
func NewDeck(isShuffled bool, cards []Card) Deck {
	deck := Deck{Id: uuid.New(), Cards: cards, IsShuffled: isShuffled}

	if len(deck.Cards) == 0 {
		deck.Cards = defaultCards()
	}

	if deck.IsShuffled {
		deck.Shuffle()
	}

	return deck
}

// Returns the number of cards that can be drew from the deck.
func (deck *Deck) Remaining() int {
	return len(deck.Cards)
}

// Shuffles the list of cards using math/rand.
func (deck *Deck) Shuffle() {
	rand.Seed(time.Now().UnixNano())
	rand.Shuffle(len(deck.Cards), func(i, j int) {
		deck.Cards[i], deck.Cards[j] = deck.Cards[j], deck.Cards[i]
	})

	deck.IsShuffled = true
}

var numberOfCardsZeroError = errors.New("Number of cards to draw must be greater than zero")
var numberOfCardsInventoryError = errors.New("There are not enough cards to draw")

// Removes the first N cards from the deck and returns removed cards. If there are not enough
// cards to draw from the deck, returns error.
func (deck *Deck) Draw(number int) ([]Card, error) {
	if number <= 0 {
		return nil, numberOfCardsZeroError
	}

	if number > deck.Remaining() {
		return nil, numberOfCardsInventoryError
	}

	drew := deck.Cards[:number]
	deck.Cards = deck.Cards[number:]

	return drew, nil
}

// Returns a slice with the standard 52-card game
// TODO: this function could be memoized or made static
func defaultCards() []Card {
	cards := make([]Card, 0, len(cardValues)*len(cardSuits))

	for _, suit := range cardSuits {
		for _, value := range cardValues {
			cards = append(cards, Card{Suit: suit, Value: value})
		}
	}

	return cards
}
