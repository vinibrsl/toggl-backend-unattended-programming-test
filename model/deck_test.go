package model

import (
	"errors"
	"github.com/google/go-cmp/cmp"
	"testing"
)

func TestNewDeckWithDefaultOptions(test *testing.T) {
	got := NewDeck(false, nil)
	expectedCards := defaultCards()

	if !cmp.Equal(got.Cards, expectedCards) {
		test.Fatalf("expected to be %v, but got %v", expectedCards, got.Cards)
	}
	if got.IsShuffled != false {
		test.Fatal("expected IsShuffled to be false")
	}
}

func TestNewDeckWithEmptyCardsList(test *testing.T) {
	got := NewDeck(false, []Card{})
	expectedCards := defaultCards()

	if !cmp.Equal(got.Cards, expectedCards) {
		test.Fatalf("expected to be %v, but got %v", expectedCards, got.Cards)
	}
	if got.IsShuffled != false {
		test.Fatal("expected IsShuffled to be false")
	}
}

func TestNewDeckShuffled(test *testing.T) {
	got := NewDeck(true, nil)
	expectedCards := defaultCards()

	if len(got.Cards) != 52 {
		test.Fatalf("expected cards count to be 52 but got %v", len(got.Cards))
	}
	if !got.IsShuffled {
		test.Fatalf("expected IsShuffled to be true")
	}
	if cmp.Equal(got.Cards, expectedCards) {
		test.Fatalf("expected cards to be in different order")
	}
}

func TestNewDeckWithCustomCards(test *testing.T) {
	card1, _ := NewCard("10H")
	card2, _ := NewCard("2H")
	customCards := []Card{card1, card2}
	got := NewDeck(false, customCards)

	if len(got.Cards) != len(customCards) {
		test.Fatalf("expected cards count to be %v but got %v", len(customCards), len(got.Cards))
	}
	if got.IsShuffled {
		test.Fatal("expected IsShuffled to be false")
	}
}

func TestNewDeckWithShuffledCustomCards(test *testing.T) {
	customCards := defaultCards()[:10]
	got := NewDeck(true, customCards)

	if cmp.Equal(got.Cards, defaultCards()[:10]) {
		test.Fatalf("expected cards to be in different order")
	}
	if len(got.Cards) != 10 {
		test.Fatalf("expected cards count to be 10 but got %v", len(got.Cards))
	}
	if !got.IsShuffled {
		test.Fatal("expected IsShuffled to be true")
	}
}

func TestDraw(test *testing.T) {
	deck := NewDeck(false, nil)

	if len(deck.Cards) != 52 {
		test.Fatalf("expected cards count to be 52 but got %v", len(deck.Cards))
	}

	_, err := deck.Draw(0)
	if !errors.Is(err, numberOfCardsZeroError) {
		test.Fatal("expected to have returned error")
	}

	_, err = deck.Draw(-1)
	if !errors.Is(err, numberOfCardsZeroError) {
		test.Fatal("expected to have returned error")
	}

	drew, err := deck.Draw(50)
	if err != nil {
		test.Fatalf("expected success got error %v", err)
	}
	if len(drew) != 50 {
		test.Fatalf("expected to have drew 50 cards got %v", len(drew))
	}

	drew, err = deck.Draw(2)
	if err != nil {
		test.Fatalf("expected success got error %v", err)
	}
	if len(drew) != 2 {
		test.Fatalf("expected to have drew 2 cards got %v", len(drew))
	}

	_, err = deck.Draw(1)
	if !errors.Is(err, numberOfCardsInventoryError) {
		test.Fatalf("expected to have returned error")
	}
}

func TestDrawValue(test *testing.T) {
	deck := NewDeck(false, nil)
	drew, err := deck.Draw(1)
	if err != nil {
		test.Fatalf("expected success got error %v", err)
	}

	if len(drew) != 1 {
		test.Fatalf("expected to have drew 1 card got %v", len(drew))
	}

	firstStandardDeckCard := defaultCards()[0]
	if !cmp.Equal(firstStandardDeckCard, drew[0]) {
		test.Fatalf("expected to be %v, but got %v", firstStandardDeckCard, drew[0])
	}
}

func TestRemaining(test *testing.T) {
	deck := NewDeck(false, nil)
	got := deck.Remaining()
	expected := 52

	if got != expected {
		test.Fatalf("expected to be %v, but got %v", expected, got)
	}

	_, err := deck.Draw(1)
	if err != nil {
		test.Fatalf("expected success got error %v", err)
	}

	got = deck.Remaining()
	expected = 51

	if got != expected {
		test.Fatalf("expected to be %v, but got %v", expected, got)
	}
}
