package model

import (
	"errors"
	"testing"
)

func TestNewCard(test *testing.T) {
	card, err := NewCard("10H")
	if err != nil {
		test.Fatalf("expected success got error %v", err)
	}
	if card.Suit != "H" {
		test.Fatalf("expected card suit to be %v got %v", "H", card.Suit)
	}
	if card.Value != "10" {
		test.Fatalf("expected card suit to be %v got %v", "10", card.Value)
	}

	card, err = NewCard("AS")
	if err != nil {
		test.Fatalf("expected success got error %v", err)
	}
	if card.Suit != "S" {
		test.Fatalf("expected card suit to be %v got %v", "S", card.Suit)
	}
	if card.Value != "A" {
		test.Fatalf("expected card suit to be %v got %v", "A", card.Value)
	}

	_, err = NewCard("XY")
	if !errors.Is(err, invalidCardFormatError) {
		test.Fatal("expected to have returned error")
	}

	_, err = NewCard("3Ç")
	if !errors.Is(err, invalidCardFormatError) {
		test.Fatal("expected to have returned error")
	}

	_, err = NewCard("𠜎𠜱𠝹𠱓𠱸𠲖𠳏")
	if !errors.Is(err, invalidCardFormatError) {
		test.Fatal("expected to have returned error")
	}
}
