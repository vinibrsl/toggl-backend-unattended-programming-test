package main

import (
	"vinibrasil.com/toggl_assessment/api"
)

func main() {
	api.RunServer()
}
