module vinibrasil.com/toggl_assessment

go 1.18

require (
	github.com/google/go-cmp v0.5.8
	github.com/google/uuid v1.3.0
	github.com/gorilla/mux v1.8.0
)
